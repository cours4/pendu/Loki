# Loki

## What is it ?
Loki is a project where we can manage the attendance of students to their courses.  

This project was realized in the framework of the module Info0303 of the university of Reims.


## Framework
<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="200"></a></p>

The projet is realized under Laravel 8.0.

## Authors

- Corentin - Student
- Owen - Student

## Prerequisites

-

## Installation
